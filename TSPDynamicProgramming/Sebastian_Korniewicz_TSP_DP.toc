\select@language {polish}
\select@language {polish}
\contentsline {section}{\numberline {1}Problem komiwoja\IeC {\.z}era (TSP)}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Przegl\IeC {\k a}d zupe\IeC {\l }ny}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Programowanie dynamiczne}{2}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Pseudokod}{3}{subsubsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}Przyk\IeC {\l }adowy przebieg algorytmu}{3}{subsubsection.1.2.2}
\contentsline {subsubsection}{\numberline {1.2.3}Opis implementacji algorytmu}{5}{subsubsection.1.2.3}
\contentsline {section}{\numberline {2}Wyniki eksperymentu}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Tabela}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Wykres}{6}{subsection.2.2}
\contentsline {section}{\numberline {3}Podsumowanie i wnioski}{6}{section.3}
\contentsline {section}{\numberline {4}Literatura}{6}{section.4}
