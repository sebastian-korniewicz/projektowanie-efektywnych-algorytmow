#pragma once
#include <fstream>

class TimeCounter
{
	long double PCFreq = 0.0;
	__int64 CounterStart = 0;
public:
	TimeCounter();
	~TimeCounter();
	void StartCounter();
	long double GetCounter();
};

