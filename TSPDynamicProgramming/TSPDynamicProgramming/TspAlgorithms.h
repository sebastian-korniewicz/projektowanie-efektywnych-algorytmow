#pragma once
#include  "SquareMatrix.h"

class TspAlgorithms
{
	int **distances;
	char **parents;
	SquareMatrix matrix;
	unsigned size;
	void generatePermutation(unsigned k, unsigned *permutation, unsigned *minPerm, unsigned &minDist);
	unsigned calcDist(unsigned *permutation);
public:
	TspAlgorithms();
	~TspAlgorithms();
	unsigned* BruteForce(SquareMatrix& matrix);
	int* DynamicProgramming(SquareMatrix& matrix);
};

