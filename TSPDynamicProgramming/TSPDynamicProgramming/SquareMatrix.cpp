#include "stdafx.h"
#include "SquareMatrix.h"

#include <ctime>
#include <fstream>
#include <stdlib.h>     /* srand, rand */

using namespace std;

void SquareMatrix::deleteValues()
{
	if (size > 0)
	{
		for (int i = 0; i < size; i++)
			delete[] values[i];
		delete[] values;
	}
}

void SquareMatrix::initialMatrix(unsigned size)
{
	this->size = size;
	if(size==0)
		return;
	values = new int *[size];
	for (unsigned i = 0; i < size; i++)
	{
		values[i] = new int[size];
		for (unsigned j = 0; j < size; j++)
			values[i][j] = 0;
	}
}

SquareMatrix::SquareMatrix(unsigned size, int defaultValue)
{
	initialMatrix(size);

	for (unsigned i = 0; i < size; i++)
		for (unsigned j = 0; j < size; j++)
			values[i][j] = defaultValue;
}

SquareMatrix::SquareMatrix(unsigned size)
{
	SquareMatrix(size, 0);
}

SquareMatrix::SquareMatrix()
{
	size = 0;
}

unsigned SquareMatrix::GetSize()
{
	return size;
}

void SquareMatrix::Initial(unsigned size)
{
	deleteValues();
	initialMatrix(size);
}

void SquareMatrix::Initial(unsigned size, int defaultValue)
{
	Initial(size);

	for (unsigned i = 0; i < size; i++)
		for (unsigned j = 0; j < size; j++)
			values[i][j] = defaultValue;
}

int SquareMatrix::GetValue(unsigned x, unsigned y)
{
	if (x >= size || y >= size)
		return INT32_MIN;
	else
		return values[x][y];
}

void SquareMatrix::LoadDataFromFile(std::string fileName)
{
	std::ifstream file;
	file.open(fileName.c_str());
	if (!file.good())
		throw "Blad dostepu do pliku zapisu\n";
	int buff;
	file >> buff;
	Initial(buff);
	for(unsigned i = 0;i<size; i++)
		for (unsigned j = 0; j<size; j++)
		{
			file >> buff;
			if (i != j)
				values[i][j] = buff;
			else
				values[i][j] = -1;
		}
}

void SquareMatrix::SaveDataToFile(std::string fileName)
{
	std::ofstream file;
	file.open(fileName.c_str());
	if (!file.good())
		throw "Blad dostepu do pliku zapisu\n";
	int buff;
	file << size << endl;
	for (unsigned i = 0; i < size; i++)
	{
		for (unsigned j = 0; j < size; j++)
		{
			file << values[i][j] << " ";
		}
		file << "\n";
	}
}

void SquareMatrix::FillRandom(unsigned size)
{
	Initial(size);
	srand(time(NULL));
	for (unsigned i = 0; i<size; i++)
		for (unsigned j = 0; j<size; j++)
		{
			if (i != j)
				values[i][j] = rand() % 100 + 15;
			else
				values[i][j] = -1;
		}
}

void SquareMatrix::Display()
{
	system("cls");
	cout << "Wielkosc: " << size << endl;
	for (unsigned i = 0; i < size; i++)
	{
		for (unsigned j = 0; j < size; j++)
			printf("%5d ", values[i][j]);
		printf("\n");
	}
}

SquareMatrix::~SquareMatrix()
{
	deleteValues();
}
