#include "stdafx.h"
#include "TimeCounter.h"
#include <windows.h>
#include <iostream>
#include <fstream>

TimeCounter::TimeCounter()
{
}


TimeCounter::~TimeCounter()
{
}


void TimeCounter::StartCounter()
{
	LARGE_INTEGER li;
	if (!QueryPerformanceFrequency(&li))
		std::cout << "QueryPerformanceFrequency failed!\n";

	PCFreq = long double(li.QuadPart) / 1000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}
long double TimeCounter::GetCounter()
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);

	return long double(li.QuadPart - CounterStart) / PCFreq;

}