#pragma once
class Menu
{
public:
	Menu();
	~Menu();
private:

	bool _end;
	bool _endSecondMenu;
	void mainLoop();
	void start();
	void displayMainMenu();
	void choiceOfProblem();
	void testMenu();
	void Measurement() { testMenu(); };
	void symmetricTravellingSalesmanProblem() {}
	void asymmetricTravellingSalesmanProblem();
	void displayTspMenu();
};
