#include "stdafx.h"
#include "Menu.h"
#include <conio.h> 
#include "SquareMatrix.h"
#include "TspAlgorithms.h"
#include "TimeCounter.h"

using namespace std;

Menu::Menu()
{
	_end = false;
	start();
}

Menu::~Menu()
{
}

void Menu::mainLoop()
{
	displayMainMenu();
	switch (_getch())
	{
	case '1':
		choiceOfProblem();
		break;
	case '2':
		Measurement();
		break;
	default:
		_end = true;
		break;
	}
}

void Menu::start()
{
	while (!_end)
	{
		mainLoop();
	}
}

void Menu::displayMainMenu()
{
	system("cls");
	cout << " ------ Programowanie dynamiczne  ------ \n"
		<< " ------ dla problemu komiwojazera ------ \n"
		<< " ----- Autor: Sebastian Korniewicz ----- \n"
		<< " --------------------------------- \n"
		<< " 1. Testowanie poprawnosci dzialania algorytmow\n"
		<< " 2. Pomiary\n"
		<< " (Aby zakonczyc wprowadz znak rozny od 1-2)\n";
}

void Menu::choiceOfProblem()
{
	system("cls");
	cout << " ---------------- Wybor problemu -------------\n"
		<< " 1. Symetryczny problem komiwojazera<brak implementacji>\n"
		<< " 2. Asymetryczny problem komiwojazera\n";
	switch (_getch())
	{
	case '1':
		symmetricTravellingSalesmanProblem();
		break;
	case '2':
		asymmetricTravellingSalesmanProblem();
		break;
	default:
		return;
		break;
	}
}

void Menu::testMenu()
{
	TspAlgorithms tsp_algorithms;
	SquareMatrix squareMatrix = SquareMatrix();


	const int N[10] = { 3, 5, 7, 9, 10, 11, 12, 15, 20, 24 };
	const int counter[10] = { 100, 100, 100,100, 100, 100, 100, 100, 100, 100};
	TimeCounter timeCounter;
	std::fstream plik;
	plik.open("result.txt", ios::app);
	if (plik.good() == false)
	{
		std::cout << "Nie znaleziono pliku\n" << std::endl;
		return;
	}
	long double	time;
	for (int j = 9; j < 10; j++)
	{
		long double result = 0;
		plik << "DP\t" << N[j] << "\t";
		for (int i = 0; i < counter[j]; i++)
		{
			squareMatrix.FillRandom(N[j]);
			timeCounter.StartCounter();
			tsp_algorithms.DynamicProgramming(squareMatrix);
			time = timeCounter.GetCounter();
			result += time;
			plik << time << "\n";
			cout << ".";
		}
		result /= counter[j];
		cout << result << endl;
	}
	_getch();
}

void Menu::asymmetricTravellingSalesmanProblem()
{
	string nameOfFile;
	unsigned size;
	TspAlgorithms tsp_algorithms;
	SquareMatrix squareMatrix = SquareMatrix();
	int *result;
	unsigned * vertices;
	_endSecondMenu = true;
	do
	{
		system("cls");
		displayTspMenu();
		switch (_getch())
		{
		case '1': // wczytanie z pliku
			cout << "\nPodaj nazwe pliku: ";
			cin >> nameOfFile;
			squareMatrix.LoadDataFromFile(nameOfFile);
			break;
		case '2': // losowanie danych
			cout << "\nPodaj liczbe miast: ";
			cin >> size;
			squareMatrix.FillRandom(size);
			break;
		case '3': // wyswietl macierz
			squareMatrix.Display();
			_getch();
			break;
		case '4': // przehlad zupelny
			vertices=tsp_algorithms.BruteForce(squareMatrix);
			for (unsigned i = 0; i < squareMatrix.GetSize(); i++)
				cout << vertices[i]<<"\t";
			cout << endl;
			_getch();
			break;
		case '5': // algorytm dynamiczny
			result = tsp_algorithms.DynamicProgramming(squareMatrix);
			cout << "Dlugosc sciezki: " << result[0] << endl;
			for (unsigned i = 0; i < squareMatrix.GetSize(); i++)
				cout << result[i + 1] << " << ";
			cout << result[1]<<endl;
			_getch();
			break;
		case '6': // algorytm dynamiczny
			squareMatrix.SaveDataToFile("24.txt");
			_getch();
			break;
		case '0':
			_endSecondMenu = false;
			break;
		}
	} while (_endSecondMenu);
}

void Menu::displayTspMenu()
{
	system("cls");
	cout << " 1. Wczytaj dane z pliku\n"
		<< " 2. Losowanie danych\n"
		<< " 3. Wyswietl macierz\n"
		<< " 4. Przeglad zupelny\n"
		<< " 5. algorytm oparty o programowaniu dynamicznym\n"
		<< " ---- 0. wyjscie ------------------------------\n";
}
