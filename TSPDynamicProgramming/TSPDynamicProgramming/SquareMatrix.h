#pragma once
#include <string>

class SquareMatrix
{
	friend class  TspAlgorithms;
private:
	void deleteValues();
	void initialMatrix(unsigned size);
protected:
	unsigned size;
	int **values;
public:
	SquareMatrix(unsigned size, int defaultValue);
	SquareMatrix(unsigned size);
	SquareMatrix();
	~SquareMatrix();
	unsigned GetSize();
	void Initial(unsigned size);
	void Initial(unsigned size, int defaultValue);
	int GetValue(unsigned x, unsigned y);
	void LoadDataFromFile(std::string fileName);
	void SaveDataToFile(std::string fileName);
	void FillRandom(unsigned size);
	void Display();
};

