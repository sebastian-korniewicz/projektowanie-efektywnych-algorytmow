﻿#include "stdafx.h"
#include "TspAlgorithms.h"
#include <bitset>

void TspAlgorithms::generatePermutation(unsigned k, unsigned* permutation, unsigned* minPerm, unsigned& minDist)
{
	if (k == 0)
	{
		//for (unsigned i = 0; i < size; i++)
		//	std::cout << permutation[i] << "\t";
		//std::cout << std::endl;
		unsigned dist = calcDist(permutation);
		{
			if (dist < minDist) {
				minDist = dist;
				memcpy(minPerm, permutation, size * 4);
			}
		}
	}
	else
	{
		unsigned *perm = permutation;
		for (int i = 0; i <= k; i++) {
			unsigned temp = permutation[k];
			permutation[k] = permutation[i];
			permutation[i] = temp;
			generatePermutation(k - 1, permutation, minPerm, minDist);

			temp = permutation[k];
			permutation[k] = permutation[i];
			permutation[i] = temp;
		}
	}
}

unsigned TspAlgorithms::calcDist(unsigned* permutation)
{
	unsigned distSum = 0;
	for (unsigned i = 0; i < size - 1; i++)
		distSum += matrix.values[permutation[i]][permutation[i + 1]];
	distSum += matrix.values[permutation[size - 1]][permutation[0]];
	return distSum;
}

TspAlgorithms::TspAlgorithms()
{
}


TspAlgorithms::~TspAlgorithms()
{

}

unsigned* TspAlgorithms::BruteForce(SquareMatrix& matrix)
{
	this->size = matrix.size;
	this->matrix = matrix;
	unsigned *permutation = new unsigned[size]; // ciag do permutacji - zaczynamy od ciagu 0,1,2,3,4,...,n-1
	for (unsigned i = 0; i < size; i++)  ///tworzenie pierwszej permutacji
		permutation[i] = i;
	unsigned *minPerm = new unsigned[size];
	minPerm = permutation;

	unsigned minDist = calcDist(permutation);
	generatePermutation(size - 1, permutation, minPerm, minDist);
	delete[] permutation;

	std::cout << "\nMinimalny cykl: " << minDist << std::endl;
	return minPerm;
}

int* TspAlgorithms::DynamicProgramming(SquareMatrix& matrix)
{
	int buff1, buff2;
	int max_int = 100000;
	unsigned mask1;
	this->size = matrix.size;
	unsigned max_mask = (1<<(size-1));  //2^(n-1)
	distances = new int*[size];
	parents = new char*[size];

	unsigned iter = 0;
	for (; iter < size; iter++)
	{
		distances[iter] = new int[max_mask];
		parents[iter] = new char[max_mask];
	}

	//dzialania dla maski=1
	distances[1][1]= matrix.values[0][1];
	parents[1][1] = 0;

	for(unsigned mask = 2; mask<max_mask; mask++) 
	{
		//inf o przebiegu funkcji
		//std::cout << "maska= " << std::bitset<3>(mask)<<"\n";
		for (char i = 1; i < size; i++)
		{
			//std::cout << "\\begin{itemize}\n";
			if(mask & (1<<(i-1))) // jesli i-1 bit rowny jest 1, tzn wybrany
			{
				//std::cout << "\\item ";
				//inf o przebiegu funkcji
				//std::cout << "D[" << (int)i << "][" << std::bitset<3>(mask) << "]\n";
				distances[i][mask] = max_int;
				// zapisanie maski z wyzerowanym i-tym bitem tj zbior S\{i}
				mask1 = mask ^ (1 << (i-1));
				if (mask1==0)
				{
					distances[i][mask] = matrix.values[0][i];
					parents[i][mask] = 0;
				}
				else
				{
					for (char j = 0; j < size - 1; j++)
					{
						// jesli j-ty bit rowny jest 1, tzn wybrany
						if (mask1 & (1 << j))
						{
							buff1 = distances[i][mask];
							buff2 = distances[j + 1][mask1] + matrix.values[j + 1][i];
							//inf o przebiegu funkcji
							//std::cout << "\tD[" << j + 1 << "][" << std::bitset<3>(mask1) << "] + d[" << j + 1 << "][" << (int)i << "]= " << distances[j + 1][mask1] << " + " << matrix.values[j + 1][i] << "= " << buff2 << "\n";
							if (buff2 < buff1)
							{
								parents[i][mask] = j + 1;
								distances[i][mask] = buff2;
							}
						}
					}
				}
				//printf("%2d:", i);

				//inf o przebiegu funkcji
				//std::cout << "\tD[" << (int)i << "][" << std::bitset<3>(mask) << "]=" << distances[i][mask] << "\tP="<< (int)parents[i][mask]<< "\n\n";
				//std::cout << std::bitset<4>(mask) << " " << distances[i][mask] << "\t" << (int)parents[i][mask] << "\n";
			}
		}
	}
	int* result = new int[size +1];
	result[0] = max_int;
	int* path = new int[size];
	max_mask--;
	//std::cout << "\n";
	for (iter = 1; iter < size; iter++)
	{
		//inf o przebiegu funkcji
		//std::cout << "D[" <<  iter << "][" << std::bitset<3>(max_mask) << "] + d[" << iter << "][" << 0 << "]= " << distances[iter][max_mask] << " + " << matrix.values[iter][0] << "= " << distances[iter][max_mask] + matrix.values[iter][0] << "\n";
		if (result[0] > distances[iter][max_mask] + matrix.values[iter][0])
		{
			result[0] = distances[iter][max_mask] + matrix.values[iter][0];
			path[0] = iter;
		}
	}
	
	for (iter = 1; iter < size; iter++)
	{
		path[iter] = parents[path[iter - 1]][max_mask];
		max_mask = max_mask ^ (1 << (path[iter-1]-1));
	}
	for (iter = 1; iter <= size; iter++)
	{
		result[iter] = path[size - iter];
	}
	iter = 0;
	for (; iter < size; iter++)
	{
		delete [] distances[iter];
		delete [] parents[iter];
	}
	delete distances;
	delete parents;
	return result;
}
